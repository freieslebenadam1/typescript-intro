const add = (number1, number2) => {
  return number1 + number2;
};

const printResult = result => {
  console.log(`Result: ${result}`);
}

printResult(add(5, 12));
printResult(add(5, "12"));
printResult(add("5", "12"));

let anyFunction;
let addFunction;
let printResultFunction;

export {};
