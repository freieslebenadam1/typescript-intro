class ExampleClass {
  // STATIC
  static staticProp = "Static Property";

  // CONSTRUCTOR
  constructor(customProp) {
    this.customProp = customProp;
  }

  // PUBLIC
  printMessage = message => console.log(message, this.#getStaticProp);

  changeStaticProp = () => {
    ExampleClass.staticProp = "Changed Static Property";
  };

  // PRIVATE
  #getStaticProp = () => ExampleClass.staticProp;
}

let exampleClass;

exampleClass = new ExampleClass("Custom Property");

console.log(
  ExampleClass.staticProp,
  exampleClass.printMessage("Message"),
  exampleClass.changeStaticProp()
);
