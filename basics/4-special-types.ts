// Enums
// enum Roles {
//   ADMIN, // 0
//   USER, // 1
// }

// Custom types
// type Role = "user" | "admin";

// Tuple
let role = [2, "user"];

//
const person = {
  name: "John",
  age: 30,
  hobbies: ["Sports", "Cooking"],
  role: role
};

export {};
