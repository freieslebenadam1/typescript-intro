const paragraph = document.querySelector("p")!;

let name1 = "John";
let age1 = 30;
let isAdult1 = true;
let hobbies1 = ["Sports", "Cooking"];

const name2 = "John";
const age2 = 30;
const isAdult2 = true;
const hobbies2 = ["Sports", "Cooking"];


paragraph.innerText = `
  name1: ${name1}
  age1: ${age1}
  isAdult1: ${isAdult1}
  hobbies1: ${hobbies1}
  ---
  name2: ${name2}
  age2: ${age2}
  isAdult2: ${isAdult2}
  hobbies2: ${hobbies2}
`;

export {};
