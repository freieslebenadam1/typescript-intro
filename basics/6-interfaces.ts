// Interfaces

let person;
let user;

person = {
  name: "John",
  age: 30,
  greet: () => console.log("Hello!")
};

user = {
  name: "Jane",
  age: 30,
  password: "1234"
};

export {};
