
## Co je Typescript
Není to programovací jazyk sám o sobě - spíše "superset" javascriptu, který jenom javascriptu přidává nové fičury.

Kompiluje do Javascriptu, protože prohlížeče (nebo Node.js) neumí rozjet Typescript.

Je to hlavně vývojářsky nástroj - pomáhá odchytit errory při vývoji během psaní kódu (nebo kompilace) buďto pomocí  IDE integrace, nebo ts-compileru.

Výhody Typescriptu oproti Javascriptu:
 - **Datové typy** (number, string, ...)
 - **Nadstavby** (interfaces, generics, ...)
 - **Metaprogramovací vlastnosti** (decorators, ...)
 - **Vysoká konfigurovatelnost**

## Typy

Typy se dají přidávat k proměnným, konstantám (*většinou netřeba*), funkcím, vlastnostem, parametrům atd. atd.

Javascript dokáže odchytit typové chyby při runtimu, ale Typescript během vývoje.

Všechny typy začínají malým písmenem (*oproti primitivním typovým objektům v Javascriptu, které začínají velkým písmenem*)

### Základní typy
- **any** (*jakýkoliv typ - pokud možno nepoužívat*)
- **number** (*čísla - žádné rozdělování na float/int atd..*)
- **string** (*textové řetězce*)
- **boolean** (*`true` a `false`*)
-  **object** / **{}** (*obecné objekty*)
- **Array** (*pole - flexibilní nebo striktní*)

Dejme tomu, že máme následující funkci:

```js
const add = (num1, num2) => num1 + num2;
```

a voláme ji s těmito hodnotami

```ts
const number1 = "5";
const number2 = 2.8;

console.log(add(number1, number2)); //-> "52.8"
```

`number1` je v tomto případě string a proto funkce `add` namísto sčítaní dvou čísel provede pouze spojení hodnot do stringu.

> `number1` a `number2` mohou být například hodnoty z user inputu, nebo ajax requestu, kde bychom napřímo neviděli, že jeden z nich může být string

Typescript nám může pomoct odchytit možnost tohohle erroru, když přidáme typy parametrů a návratový typ funkce `add`:

```ts
const add = (num1: number, num2: number) => num1 + num2;
```

Nyní by nám v tomhle případě Typescript vyhodil chybu u prvního argumentu volání funkce `add`, jelikož přijímá pouze number typ a proměnná `number1` je string.

> Ve vykompilovém souboru tyto typy nebudou. 

#### Inferované typy

Pokud deklarujeme proměnnou a přiřazujeme hodnotu najednou, Typescript automaticky odhadne datový typ a nemusíme ho explicitně doplňovat.

```ts
// Netřeba
const number1: number = 1;
```

```ts
// Toto stačí
const number1 = 1;
```

#### Objekty

```ts
const preson = {
  name: "John",
  age: 30
};
```

Tento objekt, Typescript automaticky inferuje jako typ `{ name: string; age: number; }`.

Pokud bychom tento objekt chtěli zobecnit, můžeme definovat základní `object` typ:

```ts
const person: object = {
  name: "John",
  age: 30
};
```

V tomhle případě ale Typescript nebude znát žádné vlastnosti z objektu `person` a pokud zkusíme nějakou z nich přečíst, vyhodí nám chybu.

Typescriptu můžeme vyhovět přidáním definice struktury tohoto objektu:

```ts
const person: { name: string; age: number; } = {
  name: "John",
  age: 30
}
```

Skončíme tedy nyní se stejnou strukturou, která byla původně automaticky inferována.

#### Pole

Pole se dají definovat dvěma způsoby:

```ts
const hobbies: string[] = ["Sports", "Cooking"];
````

nebo

```ts
const hobbies: Array<string> = ["Sports", "Cooking"];
```

`hobbies` budou přijímat pouze stringy v tomto případě.

Pokud bychom projížděli pole `hobbies` cyklem, typescript automaticky odhadne typ jednotlivých položek:

```ts
for (const hobby of hobbies) {
  console.log(hobby.toUpperCase());
}
```

Zde tedy `hobby` bude inferováno jako string, protože `hobbies` je definováno jako pole stringů (*v opačném případě by `toUpperCase()` funkce házela error*).

### Komplexní typy

Tyto typy existují pouze v Typescript (*základní Javascript jim nerozumí*).

#### Tuple

Typová definice "omezeného" pole. Kdybychom například chtěli definovat pole, které bude mít pouze dvě hodnoty a každá různý typ, použili bychom tuple:

```ts
const roles: [number, string] = [2, "author"];
```

Nyní bude pole `roles` vždycky držet tuto strukturu - tzn. nepůjde zavolat `push` (*protože jsme definovali pouze 2 položky*)

#### Enumy

Dejme tomu, že máme konstantu `role`, která drží číslo reprezentující level přístupu uživatele.

(0 = Admin, 1 = Manager, 2 = User)

```ts
const role = 3;
```

Časem můžeme zapomenout jaké jsou číselné možnosti a nebo co jaké číslo znamená. U tohoto můžeme použít enum, který "namapuje" názvy pro tyto číselné hodnoty od 0:

```ts
enum Roles = {
  ADMIN,
  MANAGER,
  USER
}
```

Nyní bychom tedy mohli místo 3 napsat:

```ts
const role = Roles.USER;
```

Pokud bychom `role` měli už v základu jako čitelnější string (`"ADMIN", "MANAGER", "USER"`) namísto číselných hodnot, můžeme i tak použít enum s hodnotami:

```ts
enum Roles = {
  ADMIN = "ADMIN",
  MANAGER = "MANAGER",
  USER = "USER"
}
```

Enumy se dají také použít jako typy samy o sobě. Kdybychom například chtěli, aby proměnná `role` mohla být pouze jedna hodnota z enumu `Roles`, můžeme ji definovat takto:

```ts
let role: Roles;

role = "TRAINEE" // -> error - not in enum Roles

role = "ADMIN" // -> success
```

### Spojené typy (Union types)

Pokud nechceme proměnnou limitovat pouze na jeden datový typ, můžeme využít spojených typů. Dejme tomu, že máme opět funkci `add`:

```ts
const add = (num1: number, num2: number) => {
  return num1 + num2;
};
```

Pokud bychom chtěli umožnit přijímaní string hodnot, můžeme definovat spojené typy tímto způsobem:

```ts
const add = (num1: number | string, num2: number | string) => {
  return num1 + num2;
};
```

Nyní budeme mít ale chybu, protože pokud jeden z parametrů přijde jako string a sečteme je spolu, nemůžeme vrátit typ number. Typescript nám tedy pomůže odhalit problém a víme, že náš další krok bude konverze parametrů do number typu:

```ts
const add = (num1: number | string, num2: number | string) => {
  return +num1 + +num2;
};
```

> Kdybychom chtěli provádět různé operace pro různé typy parametrů, už bychom museli přidat runtime řešení. (*např. přidat podmínku na typ parametru a následně provést něco jiného*).

### Přesné typy (Exact types)

Přesné hodnoty se dají také použít jako vlastní typ. Například, pokud chci, aby proměnná `number1` (číslo) byla vždy a pouze jenom hodnota `2.8`, můžu jí speciálně definovat takto:

```ts
let number1: 2.8;

number1 = 1; // -> error

number1 = 2.8 // -> success
```

U konstant nám typescript většinou bude automaticky inferovat přesné typy:

```ts
const number1 = 2.8; // -> inferred (number1: 2.8;)
```

### Vlastní typy

Pokud máme například dlouhý spojený typ, který se nám hodně opakuje, můžeme místo něj vytvořít vlastní typ (*alias*):

```ts
type Addable = number | string;
```

Nyní můžeme tento typ použít v přechozí `add` funkci:

```ts
const add = (num1: Addable, num2: Addable) => +num1 + +num2;
```

Vlastnímu typu můžeme přiřadit cokoliv - spojené typy, přesné typy, enumy, funkce, dokonce i struktury objektů (*pro ty ale radši využíváme **interface***).

### Funkce a typy

#### Návratové typy

Už predtím jsme viděli, že je možné přiřadit datové typy parametrům ve funkcích.

```ts
const add = (num1: number, num2: number) => num1 + num2;
```

K funcím můžeme ale přiřadit také návratový typ. Ve většině případů typescript automaticky inferuje návratový typ, ale pokud chceme být přesní, můžeme ho definovat manuálně:

```ts
const add = (num1: number, num2: number): number => num1 + num2;
```

> Vždy je lepší nechat typescript inferovat typy, ale pokud se snažíme provádět náročnější operace, nebo pokud chceme přesný typ, který typescript nedokáže odhadnout, použili bychom definici návratového typu.

##### Void

Pokud chceme, aby funkce pouze provedla nějakou operaci a nic nevracela, můžeme použít speciální typ **void**:

```ts
const printMessage = (message: string): void => console.log(message);
```

> Typescript přemýšlí o funkcích trochu jinak než Javascript. Kdybychom si lognuli tuhle `printMessage` funkci, vrátí se nám `undefined`. Kdybychom ale nastavili návratový typ jako undefined, typescript by to viděl jako error. Pouze v případě, že bychom vysloveně přidali `return undefined` (tedy návratová funkce) by toto bylo správné. Pokud ale funkcí nechceme vyloženě nic vracet, použijeme **void**.

##### Typy funkcí

U funkcí můžeme také definovat typ samotné funkce. Dejme tomu, že máme proměnnou `combineValues`, do které chceme uložit pouze hodnotu funkce, můžeme využít Function typ:

```ts
let combineValues: Function;

combineValues = 2; // -> error

combineValues = printMessage; // -> success
combineValues = add; // -> success
```

Můžeme být ještě víc specifičtější a definovat přesnou strukturu funkce s parametry a návratovým typem:

```ts
let combineValues: (num1: number, num2: number) => number;

combineValues = printMessage // -> error

combineValues = add; // -> success
```

`combineValues` tady definujeme jako funci, která bere `num1` a `num2` jako parametry typu number a vrací typ number.

Kdybychom chtěli například použít callback funkci jako parametr ve funkci `addAndHandle`, musíme definovat její typ:

```ts
const addAndHandle = (
  num1: number,
  num2: number,
  callback: (result: number) => void
) => {
  const result = num1 + num2;
  callback(result);
};
```

Zde definujeme, že parametr `callback` bude typu funkce, která bere parametr `result` jako number a nebude nic vracet.

### Neznámé typy

V typescriptu můžeme použít různé neznámé typy, pokud nevíme přesnou hodnotu.

#### Any / Unknown

**Any** a **unknown** jsou si velmi podobné na pár rozdílů. Hlavní rozdíl je, že při definici **any** říkáme typescriptu, že hodnota může být cokoliv a ať ho nic nezajímá.

```ts
let userInput: any;
let userName: string;

userInput = 24; // -> success
userInput = "John"; // -> success

userName = userInput // -> success
```

Kdybychom zde místo **any** použili **unknown**, typescript nám nedovolí přiřadit hodnotu proměnné `userName`, protože nemůže přesně vědět jaký typ bude mít:

```ts
let userInput: unknown;
let userName: string;

userInput = 24; // -> success
userInput = "John" // -> success

userName = userInput // -> error (unknown unassignable to string)
```

Tento přístup je bezpečnější než **any**, protože díky erroru z Typescriptu víme, že budeme muset přidat runtime podmínku pro typ proměnné `userInput`:

```ts
if (typeof userInput === "string") {
  userName = userInput // -> success
}
```

#### Never

Typ **never** se používá v případě, pokud nechceme nikdy vrátit žádnou hodnotu - většinou se jedná o funkce pro error handlování, které nám shodí scripty (pokud nevyužijeme try/catch)

```ts
const throwError = (message: string): never => {
  throw { message };
};

throwError("An error occurred!"); // -> never returns - crashes script
```

> V tomto případě by Typescript inferovat typ **void** (**never** je novější typ a v tomto případě přesnější, proto bude nejlepší definovat manuálně).



## Interface

Interface jsou ve zkratce struktury objektů. Díky nim můžeme popsat, jak by objekt měl vypadat - jeho vlastnosti a typy.

Například kdybychom chtěli vytvořit strukturu objektu `person`, uděláme to nějak takhle:

```ts
interface Person {
  name: string;
  age: number;
  greet: () => void;
}
```

Zde definujeme, že typ `Person` musí mít vlastnosti `name`, `age` a `greet`.

Nyní můžeme vytvářet objekty dle této struktury:

```ts
const person: Person = {
  name: "John",
  age: 30,
  greet: () => {
    console.log("Hello!");
  }
};
```

Typescript nás v tomhle případě bude hlídat, abychom vyhověli struktuře `Person`. Kdybychom například vynechali funkci `greet`, vyhodil by error.

> Interface struktury se nekompilují do Javascriptu v žádné kapacitě. Slouží nám pouze k čístému strukturování a kontrolování kódu při vývoji.

### Volitelné vlastnosti

Pokud chceme v interface nastavit vlastnost jako volitelnou, použijeme znak `?`:

```ts
interface Person {
  name: string;
  age?: number;
  greet?: () => void;
}
```

Nyní máme možnost vytvořit objekty typu `Person` pouze s `name` vlastností, bez nutnosti nastavit `age` a `greet`:

```ts
const person: Person = {
  name: "John",
  age: 30
}
```

> Tento *optional* zápis můžeme použít také například v parametrech funkcí :
```ts
const message = (text: string, print?: boolean): string => {
  if (print) {
    console.log(text);
  }

  return text;
}
```

### Slučování Interfaců

Pokud máme několik společných struktur napříč různými interfacy, můžeme využít buďto **sloučení** nebo **rozšiřování**.

#### Rozšiřování

Například pokud chceme mít více interfaců, které mají vlastnosti `id` a `name` můžeme využít rozšíření pomocí klíčového slova `extends`:

```ts
interface WithIdAndName {
  id: number;
  name: string;
}

interface Token extends WithIdAndName {
  token: string;
}

interface User extends WithIdAndName {
  password: string;
}
```

Nyní mají obojí `Token` a `User` jako součást všechny vlastnosti ze struktury `WithIdAndName`.

```ts
const user: User = {
  password: "1234"
}
// -> error - (id and name missing)
```

> Je možné rozšířit několik interfaců, pokud je oddělíme čárkou: `extends WithId, WithName`.

#### Sloučení (Intersection)

Jako druhou možnost můžeme využít slučování interfaců pomocí znaku `&` v definici vlastního typu:

```ts
interface WithId {
  id: number;
}

interface Token {
  token: string;
}

type TokenWithId = Token & WithId;
```

### Definice funkcí pomocí Interface

Už víme, že můžeme definovat typ funkcí:

```ts
type Add = (num1: number, num2: number) => number;

const add: Add = (a, b) => a + b;
```

K jejich definici ale můžeme využít také interface:

```ts
interface Add {
  (num1: number, num2: number): number;
}
```

Výhodou tohoto je, že můžeme přidat více definic této funkce (tedy přetěžovat):

```ts
interface Add {
  (num1: number, num2: number): number;
  (num1: string, num2: number): number;
  (num1: number, num2: string): number;
  (num1: string, num2: string): string;
}
```

> Typové rozdíly v takto přetěžovaných funkcích už je runtime problém, tudíž budeme muset řešit použitím javascriptových type podmínek.


## Třídy

Typescript přidává nové možnosti jak psát třídy.

### Přístupové metody

V Javascriptu se běžně využívají **public** metody/vlastnosti. Nově můžeme použít i **private**, s `#` prefixem. Dále můžeme použít také **static** pro metody/vlastnoti nezávislé na instanci dané třídy.

```js
class SampleClass {
  // STATIC
  static staticProp = "Static Property";

  // PUBLIC
  printMessage = message => console.log(message);

  // PRIVATE
  #getStaticProp = () => SampleClass.staticProp;
}
```

Typescript je dělá trochu srozumitelnější - nyní můžeme použít přímo klíčová slova pro všechny accessory:

```ts
class SampleClass {
  // STATIC
  static staticProp = "Static Property";

  // PUBLIC
  public printMessage = message => console.log(message);

  // PRIVATE
  private getStaticProp = () => SampleClass.staticProp;

  // PROTECTED
  protected printStaticProp = () => console.log(this.getStaticProp());
}
```

#### Protected

Protected je nový accessor přístupný pouze v Typescriptu. Funguje podobně jako **private** - nemůžeme k němu přistupovat mimo třídu.

Oproti **private** můžeme ale k **protected** přistupovat ve třídách které extendují (*dědí*) tuto třídu.

```ts
class Person {
  protected age = 30;
  private alive = true;
}

const person = new Person();

console.log(person.age); // -> error -> undefined
```

```ts
class User extends Person {
  public getAge = () => this.age; // visible
  public isAlive = () => this.alive; //-> error -> not visible
}

const user = new User();

console.log(user.getAge()) // -> success -> 30
```

#### Readonly

Další modifikátor, který Typescript přidává je `readonly`. Používá se v kombinaci s ostatními přístupovými modifikátory a díky němu můžeme znepřístupnit možnost přepsat hodnotu určité vlastnosti (*bude pouze ke čtení*).

```ts
class Person {
  public name = "John";
  public readonly age = 30;
}

const person = new Person();

person.name = "Bob"; // -> succes -> "Bob"
person.age = 33; // -> error -> readonly
```

### Konstruktor

V javascriptu můžeme s pomocí konstruktoru přiřadit hodnoty do instance třídy:

```js
class SampleClass {
  constructor(name) {
    this.name = name;
  }
}
```

V tomto případě bychom přiřadili hodnotu parametru `name` do public vlastnosti `name` v instanci třídy. V typescriptu toto bude trochu komplexnější kvůli tomu, že musíme přemýšlet nad přístupovými metodami a typy jednotlivých párů parametr - vlastnost.

Proto by předchozí příklad v typescriptu házel chyby. První chyba bude kvůli "neexistenci" vlastnosti `name` v naší třídě a další chyba by byly neznámé typy. V typescriptu bychom to tedy napsali takto:

```ts
class Person {
  public name: string;

  constructor(name: string) {
    this.name = name;
  }
}
```

Samozřejmě při více takovýchto vlastností už by se nám dost roztahoval kód, proto Typescript umožňuje tento celý zápis velmi zkrátit:

```ts
class Person {
  constructor(public name: string) {}
}
```

Touto syntaxí říkáme, že v konstruktoru přijde parametr `name`, který bude typu string a chceme ho uložit jako veřejnou vlastnost instancí této třídy. Stejný zápis můžeme použít i pro **private** a **protected** vlastnosti:

```ts
class Person {
  constructor(
    public name: string,
    private age: number,
    protected surname: string,
  ) {}
}
```

### Třída jako typ

V typescriptu můžeme také používat třídy jako typ k proměnným

```ts
let person: Person;

person = new Person();
```

### Abstraktní třídy

Abstraktní třídy jsou třídy, které obecně slouží pouze pro účel dědičnosti - nemůžeme vytvořit jejich instanci, ale můžeme je dědit jinou třídou. Abstraktní třídu můžeme vytvořit s použitím klíčového slova **abstract** u deklarace třídy:

```ts
abstract class Human {
  abstract public name: string;
  protected age = 30;

  protected sayHello = () => {
    return `Hello, my name is ${this.name}, and I'm ${this.age} years old.`;
  }
}
```

Všiměte si klíčového výrazu `abstract` také u vlastnosti `name`. U jednotlivých vlastností/metod v abstraktních třídách můžeme vyžadovat jejich nutnou implementaci tímto způsobem. Těmto abstraktním částem nepřidáváme žádné hodnoty - pouze definujeme, jak mají vypadat.

Například nyní, kdybychom vytvořili třídu `Person`, která bude dědit abstraktní třídu `Human`, budeme muset implementovat vlastnost `name`, jinak nám typescript vyhodí chybu.

```ts
class Person extends Human {
  public name = "John";

  constructor() {
    this.sayHello();
  }
}

new Person() // -> "Hello, my name is John, and I'm 30 years old."
```

### Singleton třídy

Typescript nám umožňuje vytvořit tzv. singleton třídu - třída, která vytvoří pouze jednu jedinou instanci.

Uděláme to tak, že konstruktor nastavíme jako **private**, což nám znepřístupní možnost vytvářet instance této třídy.

```ts
class Singleton {
  private constructor() {}
}
```

S touto třídou následně můžeme pracovat pouze s použitím statických metod - tzn. instanci třídy vytvoříme pomocí statické metody a ve statické vlastnosti budeme držet tuto instanci:

```ts
class Singleton {
  private static instance: Singleton;

  private constructor() {}

  static getInstance = () => {
    if (!this.instance) {
      this.instance = new Singleton();
    }

    return this.instance();
  };
}
```

Tímto způsobem bude existovat pouze jedna jediná instance a budeme k ní moct přistupovat tímto způsobem:

```ts
const singleton = Singleton.getInstance();
const sameSingleton = Singleton.getInstance();

console.log(singleton === sameSingleton); // -> true
```

### Využití Interfaců s třídami

Ve třídách můžeme implementovat interface struktury, kterými definujeme vlastnosti a metody dané třídy. 

```ts
interface Person {
  name: string;
  age: number;
  greet: () => void;
}

class User implements Person {
  public name = "John";
  private age = 30;

  public greet = () => {
    console.log(`Hello, my name is ${this.name}, and I'm ${this.age} years old.`);
  };
}
```

Tento způsob je velmi podobný abstraktním metodám. Hlavní rozdíl je, že v abstraktních metodách můžeme kromě abstraktních struktur implementovat také hodnoty, které následně tato třída může využít. Interfacy mohou pouze definovat strukturu toho, co je potřeba implementovat v této třídě.

Výhoda využívání interfaců u tříd je, že oproti dedičnosti můžeme implementovat více různých interfaců. Dědit jinou třídu můžeme i s interface implemetací.

```ts
class Example extends Core implements Callable, Interactable {
  // ...
}
```

Tímto způsobem budeme mít třídu `Example`, která bude dědit třídu `Core` a implementovat (*dodržovat*) struktury z `Callable` a `Interactable` interface.

Díky interface strukturám máme možnost využívat určité vlastnosti různých tříd bez nutnosti použít třídu samotnou jako typ. Například pokud bychom měli třídy `Person` a `Human` a obě by měly obsahovat `greet` metodu:

```ts
interface Greetable {
  greet: () => void;
}

class Person implements Greetable {
  greet = () => "Hello!";
}

class Human implements Greetable {
  greet = () => "Good morning!";
}
```

můžeme vytvořit proměnnou pouze dle `Greetable` interface, kterému budou vyhovovat obě třídy:

```ts
let user: Greetable;

user = new Person(); // -> success
user = new Human(); // -> success

user.greet(); // -> success
```


## Pokročilý Typescript

### Typové podmínky (Type guards)

Když chceme provádět různé operace dle různých typů, bude se jednat o runtime problém - tedy musíme využít javascriptové řešení.

#### Základní typy

```js
typeof myVariable // -> "string" | "number" | "boolean" | "object" | ...
```

Pokud chceme zjistit, jestli proměnná je určitého typu, můžeme použít `typeof`:

```ts
const printMessage = message => {
  if (typeof message === "string") {
    console.log(message);
  }
};
```

> Zde můžeme porovnávat pouze základní javascriptové typy (string, number, object, boolean, ...). Nemůžeme tedy například porovnat hodnotu Interfacu nebo vlastního typu, protože ty klasický javascript nezná.

#### Objekty

```js
"property" in someObject // -> boolean
```

Pokud bychom měli spojený typ dvou interfaců:

```ts
interface Admin {
  name: string;
  privileges: string[];
}

interface Employee {
  name: string;
}

type UnknownEmployee = Employee | Admin;
```

`UnknownEmployee` zde může být buďto `Admin` nebo `Employee`. Kdybychom tedy chtěli využít tento spojený typ, budeme muset dát pozor, aby dotyčná struktura obsahovala vlastnost, se kterou chceme pracovat. 

```ts
const printEmployeeInfo = (employee: UnknownEmployee) => {
  console.log(`Name: ${employee.name}`);
  console.log(`Name: ${employee.privileges.join(", ")}`);
};
```

Toto by typescript vyhodnotil jako chybné, protože nemůže odhadnout přesný typ parametru `employee`.

`name` zde mají obě struktury, ale pokud bychom chtěli použít `privileges` (pouze v `Admin`), budeme muset nejdřív přidat podmínku pro existenci této vlastnosti uvnitř dotyčného objektu:

```ts
const printEmployeeInfo = (employee: UnknownEmployee) => {
  console.log(`Name: ${employee.name}`);

  if ("privileges" in employee) {
    console.log(`Name: ${employee.privileges.join(", ")}`);
  }
};
```

#### Typ instance

Další typ podmínkování platí pro instance tříd.

```js
myInstance instanceof MyClass // -> boolean
```

Takto můžeme kontrolovat pokud naše proměnná je instancí třídy. Pozor, toto funguje pouze pro třídy. Třídy se vykompilují do javascriptu - pokud bychom toto zkusili použít pro vlastní typy nebo interfacy toto nebude fungovat.

Podobný příklad jako předtím:

```ts
class Car {
  drive = () => {
    console.log("Driving...");
  };
}

class Truck {
  drive = () => {
    console.log("Driving a truck...");
  };

  loadCargo = () => {
    console.log("Loading cargo...");
  };
}

type Vehicle = Car | Truck;
```

```ts
const useVehicle = (vehicle: Vehicle) => {
  vehicle.drive();
  vehicle.loadCargo();
};
```

Tato funkce bude opět chybná, protože `loadCargo` metoda se vyskytuje pouze ve třídě `Truck`. Opět bychom mohli použít `("loadCargo" in vehicle)`, ale jelikož se jedná o třídy a je tu možnost překlepu v `"loadCargo"`, bude dávat větší smysl využití `instanceof` podmínky:

```ts
const useVehicle = (vehicle: Vehicle) => {
  vehicle.drive();

  if (vehicle instanceof Truck) {
    vehicle.loadCargo();
  }
};
```

### Připsané typy (Type casting)

Pokud chceme v DOMu vyselectovat nějaký element, můžeme použít query selector:

```ts
const paragraph = document.querySelector("p");
```

V tomto případě typescript dokáže správně inferovat, že se bude jednat o paragraf element (protože selector "p" vybere jenom jedině `<p>` element).

Co kdybychom ale selectovali element dle `id` nebo `class` atributu?

```ts
const paragraph = document.querySelector(".some-unknown-class");
```

V tomto okamžiku Typescript nemá žádnou možnost vědět jaký element přesně dostane. Třída `some-unknown-class` může být na divu, paragrafu, section, a vlastně úplně všech možných elementů.

Kdybychom chtěli například změnit text uvnitř tohoto elementu pomocí:

```ts
paragraph.innerText = "Lorem Ipsum";
```

Typescript vyhodí chybu, protože neví přesně jakého typu je `paragraph` - tudíž neví, jestli obsahuje vlastnost `innerText`.

V těhlech případech můžeme Typescriptu pomoct porozumět kontextu pomocí připsání typu pomocí klíčového slova `as`

```ts
const paragraph = document.querySelector(".some-unknown-class") as HTMLParagraphElement;
```

Nebo pomocí definice **generického** typu:

```ts
const paragraph = <HTMLParagraphElement>document.querySelector(".some-unknown-class");
```

> Nedoporučuje se při používání JSX syntaxe - např. React / Preact.

> Je důležité zdůraznit, že tímto způsobem "násilně" řekneme o jaký typ se jedná. Typescript nám při použití type castingu bude věřit a záleží na nás, abychom měli ověřené, že se opravdu jedná o element, který mu radíme.

### Generické typy (Generic types)

Generické typy jsou dynamické struktury, které nedokážeme definovat při deklaraci ale až při přiřazení.

Dobrým příkladem může být typ `Array`. Když budeme mít prázdný array určený pro jména:

```ts
const names = [];
```

Typescript automaticky inferuje typ `any[]` - tedy pole jakýchkoliv hodnot. My můžeme dodat, že to opravdu má být array...

```ts
const names: Array = [];
```

ale zde bude mít Typescript problém, protože `Array` je generický typ, který vyžaduje specifikaci vnitřních typů. To provedeme přidáním typů, které má toto pole obsahovat do `<>` znaků. V tomto případě by to vypadalo takto:

```ts
const names: Array<string> = [];
```

> Pro tento jednoduchý příklad bude lepší využít zkratku `string[]`, ale je to možné i tímto způsobem.

#### Vytvoření generické funkce

Generické typy můžeme použít i ve vlastních funkcích. Dejme tomu, že chceme mít funkci, která spojí dva objekty do jednoho.

```ts
const merge = (objectA: object, objectB: object) => {
  return Object.assign(objectA, objectB);
};
```

a díky ní vytvoříme následující objekt:

```ts
const mergedObject = merge({ name: "John" }, { age: 30 });
```

kdybychom chtěli přečíst vlastnost `name`, typescript nám vyhodí chybu, protože neví jistě, že tam ta vlastnost bude. Toto by se dalo obejít type-castingem, ale lepší by bylo obecné řešení pro všechny budoucí výskyty této funkce.

K tomu můžeme využít generických typů:

```ts
const merge = <T1, T2>(objectA: T1, objectB: T2) => {
  return Object.assign(objectA, objectB);
}
```

(Mohli bychom ještě doplnit návratový typ `T1 & T2`, ale typescript by v tomto případě měl být schopný toto sám inferovat)

Nyní už budeme schopní provést následující bez problémů:

```ts
const mergedObject = merge({ name: "John" }, { age: 30 });

console.log(mergedObject.name);
console.log(mergedObject.age);
```

Možná jednodušší příklad by bylo využití stejné struktury pro různé typy:

```ts
interface Range<T> {
  start: T;
  end: T;
}
```

Nyní mohu vytvořit range objekt pro různé typy:

```ts
const dateRange: Range<Date> = {
  start: new Date(),
  end: new Date()
}

const timeRange: Range<string> = {
  start: "08:00",
  end: "16:00"
}
```

#### Generický základ

U předchozího příkladu s `merge` funkcí nastavujeme generické typy ke spojení. Takhle ovšem ale můžeme jako argument poslat jakýkoliv typ - nejen objekty. Toto můžeme definovat klíčovým slovem `extends` u definice generických typů:

```ts
const merge = <T1 extends object, T2 extends object>(objectA: T1, objectB: T2) => {
  return Object.assign(objectA, objectB);
}
```

tímto způsobem se nemůže stát že zavoláme funkci `merge` s jinými argumenty než typu objekt.

### Dynamické indexování objektů

V případě, že chceme dostat hodnotu z objektu dle klíče, který dostáváme dynamickým způsobem - například kdybychom měli následující funkci:

```ts
const getValue = (obj: object, key: string) => {
  return obj[key];
};
```

Typescript zde vyhodí chybu, protože nemůže garantovat, že `obj` bude opravdu obsahovat `key`, který posíláme v parametru.

Pro objekt můžeme přidat generický typ:

```ts
const getValue = <T extends object>(obj: T, key: string) => {
  return obj[key];
};
```

nyní máme garantovaný dynamický objekt, ale chybu nám toto stále nevyřeší, protože stále nemůžeme zaručit, že `key` bude indexem typu `T`. Proto můžeme využít druhý generický typ `K` a definovat ho jako klíč generického typu `T` pomocí klíčového slova `keyof`:

```ts
const getValue = <T extends object, K extends keyof T>(obj: T, key: K) => {
  return obj[key];
};
```

nyní můžeme zavolat tuto funkci bez problémů:

```ts
const value = getValue({ name: "John" }, "name");
```

### Speciální Typescript typy

Typescript nám v základu nabízí pár užitečných generických typů, abychom je nemuseli vytvářet sami. Zde je pár z nich.

#### Partial

Slouží k předpřípravě objektu, který má nakonci dosáhnout určité struktury. Pro příklad:

```ts
interface Post {
  title: string;
  description: string;
  createdAt: Date;
}

const createPost = (
  title: string,
  description: string,
  createdAt: Date
): Post => {
  const post: Post = {};

  post.title = title;
  post.description = description;
  post.createdAt = createdAt;

  return post;
};
```

V této funkci Typescript vyhodí chybu při deklaraci konstanty `post`, kvůli tomu, že už od začátku nedodržuje interface strukturu `Post` (*inicializuje se jako prázdný objekt*).

Tomuto se můžeme vyhnout s využitím `Partial` generického typu. Místo definice `Post` typu použijeme `Partial<Post>` - což nahradí vlastnosti našeho `Post` interface volitelnými vlastnosti a zbavíme se tedy erroru.

Nyní dostaneme ale nový error u `return post;`, protože místo `Post` typu vracíme `Partial<Post>` typ. Můžeme tedy využít type-castingu (*protože víme, že docílíme chtěné struktury*):

```ts
const createPost = (
  title: string,
  description: string,
  createdAt: Date
): Post => {
  const post: Partial<Post> = {};

  post.title = title;
  post.description = description;
  post.createdAt = createdAt;

  return post as Post;
};
```

#### Readonly

Tento generický typ můžeme použít pokud chceme zamezit možnost upravovat hodnoty. Dobrým příkladem může být pole stringů:

```ts
const names = ["John", "Jane"];
```

Typescript zde automaticky inferuje typ `string[]`, což je v pořádku. Co když ale chceme zamezit přidávání hodnot do tohoto pole?

```ts
names.push("Bob");
```

K tomu slouží `Readonly` generický typ:

```ts
const names: Readonly<string[]> = ["John", "Jane"];

names.push("Bob"); // -> error - readonly
```
