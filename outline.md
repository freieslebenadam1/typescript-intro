| Základy | Pokročilé |
| - | - |
| [0. Co a k čemu je Typescript](#0-co-a-k-čemu-je-typescript-zpátky) | [1. Typy](#1-typy-zpátky) |
| [1. Základní typy](#1-základní-typy-zpátky) | [2. Funkce](#2-funkce-1-zpátky) |
| [2. Typové fičury](#2-typové-fičury-zpátky) | [3. Třídy](#3-třídy-1-zpátky) |
| [3. Funkce](#3-funkce-zpátky) | [4. Type-guards](#4-type-guards-zpátky) |
| [4. Speciální typy](#4-speciální-typy-zpátky) | [5. Generické typy](#5-generické-typy-zpátky) |
| [5. Interface](#5-interface-zpátky) | [6. Zmínky](#6-zmínky-zpátky) |
| [6. Třídy](#6-třídy-zpátky) | |

# Základy

## 0. Co a k čemu je Typescript ([*Zpátky*](#))
- javascript superset
- prohlížeče neznají typescript
- vývojářský nástroj (compiler / IDE)

## 1. Základní typy ([*Zpátky*](#))
- number
- string
- boolean
- pole[]
- object
- any

## 2. Typové fičury ([*Zpátky*](#))
- Inferované typy
- union typy
- type-casting
- Přesné typy

## 3. Funkce ([*Zpátky*](#))
- parametry
- návratový typ
- typ funkce samotné (`Function` vs přesný typ)
- VOID

## 4. Speciální typy ([*Zpátky*](#))
- Tuple
- Enumy
- Vlastní typy - aliasy

## 5. Interface ([*Zpátky*](#))
- struktury
- optional vlastnosti
- extends (multiple)

## 6. Třídy ([*Zpátky*](#))
- access modifiers
- protected
- readonly
- constructor shorthand
- třída jako typ

# Pokročilé

## 1. Typy ([*Zpátky*](#))
- unknown
- never
- Partial\<T>
- Readonly\<T>

## 2. Funkce ([*Zpátky*](#))
- interface typ funkce (přetěžování)

## 3. Třídy ([*Zpátky*](#))
- abstraktní třídy
- singleton
- implements interface

## 4. Type-guards ([*Zpátky*](#))
- typeof ..
- .. in ..
- .. instanceof ..

## 5. Generické typy ([*Zpátky*](#))
- T (T1, T2)
- T extends object
- K extends **keyof** T

## 6. Zmínky ([*Zpátky*](#))
- (Dekorátory)
- (Moduly / Namespacy)
- (specifický typ `type` v interfacu)