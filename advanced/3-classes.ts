// -- Abstract Classes
class Human {
  protected name: string = "John";
  protected age: number = 30;

  greet = (): void => {
    console.log(this.greetMessage(this.name));
  };

  protected greetMessage = (name: string) => {
    return `Hello, I'm ${this.name}!`;
  };
}

class Person extends Human {
  constructor() {
    super();
    this.name = "Jane";

    this.greet();
  }
}

const human = new Human();
const person = new Person();



// -- Classes and Interfaces
interface WithName {
  name: string;
}

interface WithAge {
  age: number;
}

interface Greetable {
  greet: () => void;
}

class User {
  constructor() {}
}

const user = new User();




// -- Singleton Class
class Singleton {
  private instance: any;

  constructor() {}

  public getInstance = () => this.instance;

  public printMessage = (message: string) => console.log(message);
}

const singleton = new Singleton();
singleton.printMessage("Hello!");

export {};
