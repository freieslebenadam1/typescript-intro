interface Test {
  available: boolean;
}

interface Identifiable {
  id: string;
  token?: string;
}

const stringValue = "Hello";
const numberValue = 5;

const test: Test = { available: true };
const identifiable: Identifiable = { id: "1" };

const today = new Date();
const stringObject = new String("Hello");


// -- typeof
console.log(
  typeof stringValue, // -> string;
  typeof numberValue, // -> number;
  typeof test,        // -> object;
  typeof identifiable // -> object;
);


// -- in
console.log(
  "available" in test,    // -> true;
  "id" in identifiable,   // -> true;
  "token" in identifiable // -> false;
);


// -- instanceof
console.log(
  today instanceof Date,         // -> true;
  stringObject instanceof String // -> true;
);


export {};
