type Processable = string;

function processInput(input: Processable): void {
  console.log(`Processing ${typeof input}:`, input);
}

processInput("Hello");
processInput(42);
processInput([1, 2, 3]);
processInput({ name: "John" });

export {};
