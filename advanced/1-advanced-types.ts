// any / unknown
let userInput: any;
let userName: string;

userInput = 5;
userInput = "John";

userName = userInput;


// never
const throwServerError = (message: string) => {
  throw { message, code: 500 };
}

throwServerError("Error");


// Partial<T>
interface Post {
  title: string;
  content: string;
  published: boolean;
  createdAt: Date;
}

const createPost = (title: string, content: string): Post => {
  const post: Post = {};

  post.title = title;
  post.content = content;
  post.published = false;
  post.createdAt = new Date();

  return post;
};


// Readonly<T> (as const)
const threeNames = ["John", "Jane", "Jack"];

threeNames.push("Jill");

export {};
