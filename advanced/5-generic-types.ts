interface DateRange {
  start: Date;
  end: Date;
}

interface TimeRange {
  start: string;
  end: string;
}

const dates: DateRange = {
  start: new Date(),
  end: new Date()
};

const times: TimeRange = {
  start: "12:00",
  end: "13:00"
};


// --- Base type extending


const merge = (objA, objB) => {
  return { ...objA, ...objB };
};

const objectA = { name: "John" };
const objectB = { age: 30 };
const mergedObject = merge(objectA, objectB);

console.log(
  mergedObject.name,
  mergedObject.age
);


// --- Keof generic extended type


const getValue = (data, key) => {
  return data[key];
};

const data = { name: "John", age: 30 };
const name = getValue(data, "name");
const age = getValue(data, "age");


export {};
